package ru.omsu.vglinskii.h2database.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.omsu.vglinskii.h2database.model.Person;
import ru.omsu.vglinskii.h2database.repository.PersonRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DataInit implements ApplicationRunner {

    private PersonRepository personRepository;

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    public DataInit(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = personRepository.count();

        if (count == 0) {
            Person person1 = new Person();
            person1.setFullName("Vladimir Glinskii");
            Date birthdate1 = dateFormat.parse("1999-01-18");
            person1.setDateOfBirth(birthdate1);

            Person person2 = new Person();
            person2.setFullName("Dmitrii Vitutnev");
            Date d2 = dateFormat.parse("1999-09-06");
            person2.setDateOfBirth(d2);

            personRepository.save(person1);
            personRepository.save(person2);
        }

    }

}
