package ru.omsu.vglinskii.h2database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.omsu.vglinskii.h2database.model.Person;
import ru.omsu.vglinskii.h2database.repository.PersonRepository;

@Controller
public class MainController {

    private PersonRepository personRepository;

    @Autowired
    public MainController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @ResponseBody
    @RequestMapping("/")
    public String index() {
        Iterable<Person> all = personRepository.findAll();

        StringBuilder sb = new StringBuilder();
        all.forEach(p -> sb.append(p.getFullName() + "<br>"));

        return sb.toString();
    }

}
