package ru.omsu.vglinskii.h2database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.omsu.vglinskii.h2database.model.Person;

import java.util.Date;
import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

    List<Person> findByFullNameLike(String name);

    List<Person> findByBirthDateGreaterThan(Date date);

}
