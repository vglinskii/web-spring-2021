package ru.omsu.vglinskii.uploadfiles.forms;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileForm {

    private String description;

    private MultipartFile[] fileData;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile[] getFileData() {
        return fileData;
    }

    public void setFileData(MultipartFile[] fileData) {
        this.fileData = fileData;
    }

}
