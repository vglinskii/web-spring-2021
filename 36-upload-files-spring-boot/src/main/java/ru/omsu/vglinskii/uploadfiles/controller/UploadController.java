package ru.omsu.vglinskii.uploadfiles.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import ru.omsu.vglinskii.uploadfiles.forms.UploadFileForm;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UploadController {

    @RequestMapping(value = "/")
    public String homePage() {
        return "index";
    }

    @GetMapping(value = "/uploadOneFile")
    public String uploadOneFileHandler(Model model) {
        UploadFileForm uploadFileForm = new UploadFileForm();
        model.addAttribute("uploadFileForm", uploadFileForm);

        return "uploadOneFile";
    }

    @PostMapping(value = "/uploadOneFile")
    public String uploadOneFileHandlerPOST(
            HttpServletRequest request,
            Model model,
            @ModelAttribute("uploadFileForm") UploadFileForm uploadFileForm
    ) {
        return this.doUpload(request, model, uploadFileForm);
    }

    @GetMapping(value = "/uploadMultiFile")
    public String uploadMultiFileHandler(Model model) {
        UploadFileForm uploadFileForm = new UploadFileForm();
        model.addAttribute("uploadFileForm", uploadFileForm);

        return "uploadMultiFile";
    }

    @PostMapping(value = "/uploadMultiFile")
    public String uploadMultiFileHandlerPOST(
            HttpServletRequest request,
            Model model,
            @ModelAttribute("uploadFileForm") UploadFileForm uploadFileForm
    ) {
        return this.doUpload(request, model, uploadFileForm);
    }

    private String doUpload(
            HttpServletRequest request,
            Model model,
            UploadFileForm uploadFileForm
    ) {
        String description = uploadFileForm.getDescription();
        String uploadRootPath = request.getServletContext().getRealPath("upload");

        File uploadRootDir = new File(uploadRootPath);
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        MultipartFile[] fileData = uploadFileForm.getFileData();
        List<String> uploadedFiles = new ArrayList<>();
        List<String> failedFiles = new ArrayList<>();

        for (MultipartFile file : fileData) {
            String name = file.getOriginalFilename();
            if (name != null && name.length() > 0) {
                File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(serverFile))) {
                    bos.write(file.getBytes());
                    uploadedFiles.add(serverFile.getName());
                    System.out.println("Write file: " + serverFile);
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                    failedFiles.add(name);
                }
            }
        }
        model.addAttribute("description", description);
        model.addAttribute("uploadedFiles", uploadedFiles);
        model.addAttribute("failedFiles", failedFiles);
        return "uploadResult";
    }

}
