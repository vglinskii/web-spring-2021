package ru.omsu.vglinskii.scheduling.schedulers;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class WriteCurrentTimeSchedule {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss sss");

    @Scheduled(initialDelay = 3 * 1000, fixedDelay = 2 * 1000)
    public void writeCurrentTime() {
        String nowString = dateFormat.format(new Date());
        System.out.println("Now is: "+ nowString);
    }

    @Scheduled(cron = "*/2 * * * * *")
    public void writeCurrentTimeCron() {
        String nowString = dateFormat.format(new Date());
        System.out.println("[Cron]: Now is: "+ nowString);
    }

}
