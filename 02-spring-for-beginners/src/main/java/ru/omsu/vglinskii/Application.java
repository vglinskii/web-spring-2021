package ru.omsu.vglinskii;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.omsu.vglinskii.components.ApplicationComponent;
import ru.omsu.vglinskii.configuration.ApplicationConfiguration;
import ru.omsu.vglinskii.languages.Language;
import ru.omsu.vglinskii.service.GreetingService;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        System.out.println("----------");

        Language language = (Language) context.getBean("language");
        System.out.println("Bean Language: " + language);
        System.out.println("Call language.sayBye(): " + language.getBye());

        System.out.println("----------");

        GreetingService service = (GreetingService) context.getBean("greetingService");
        service.sayGreeting();

        System.out.println("----------");

        ApplicationComponent applicationComponent = (ApplicationComponent) context.getBean("applicationComponent");
        applicationComponent.showAppInfo();

        System.out.println("----------");
    }

}
