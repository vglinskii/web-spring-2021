package ru.omsu.vglinskii.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.omsu.vglinskii.repository.ApplicationRepository;

@Component
public class ApplicationComponent {

    private ApplicationRepository applicationRepository;

    @Autowired
    public ApplicationComponent(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public void showAppInfo() {
        System.out.println("Now is: " + applicationRepository.getSystemDateTime());
        System.out.println("App Name: " + applicationRepository.getAppName());
    }

}
