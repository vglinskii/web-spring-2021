package ru.omsu.vglinskii.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.omsu.vglinskii.languages.Language;
import ru.omsu.vglinskii.languages.Vietnamese;

@Configuration
@ComponentScan({"ru.omsu.vglinskii"})
public class ApplicationConfiguration {

    @Bean(name ="language")
    public Language getLanguage() {
        return new Vietnamese();
    }

}
