package ru.omsu.vglinskii.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.omsu.vglinskii.languages.Language;

@Service("greetingService")
public class GreetingService {

    private Language language;

    @Autowired
    public GreetingService(Language language) {
        this.language = language;
    }

    public void sayGreeting() {
        String greeting = language.getGreeting();

        System.out.println("Greeting: " + greeting);
    }

}
