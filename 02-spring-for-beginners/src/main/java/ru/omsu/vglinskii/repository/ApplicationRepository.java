package ru.omsu.vglinskii.repository;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository("applicationRepository")
public class ApplicationRepository {

    public String getAppName() {
        return "Hello Spring App";
    }

    public LocalDateTime getSystemDateTime() {
        return LocalDateTime.now();
    }

}
