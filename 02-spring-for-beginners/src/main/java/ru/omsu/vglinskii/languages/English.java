package ru.omsu.vglinskii.languages;

public class English  implements Language {

    public String getGreeting() {
        return "Hello";
    }

    public String getBye() {
        return "Bye bye";
    }

}
