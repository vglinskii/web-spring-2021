package ru.omsu.vglinskii.languages;

public interface Language {

    String getGreeting();

    String getBye();

}
