package ru.omsu.vglinskii.thymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.omsu.vglinskii.thymeleaf.configuration.AppProperties;
import ru.omsu.vglinskii.thymeleaf.form.PersonForm;
import ru.omsu.vglinskii.thymeleaf.model.Person;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PersonController {
    private static List<Person> persons = new ArrayList<>();
    private AppProperties properties;

    static {
        persons.add(new Person("Bill", "Gates"));
        persons.add(new Person("Steve", "Jobs"));
    }

    @Autowired
    public PersonController(AppProperties properties) {
        this.properties = properties;
    }

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("message", properties.getMessage());

        return "index";
    }

    @GetMapping("/personList")
    public String personList(Model model) {
        model.addAttribute("persons", persons);

        return "personList";
    }

    @GetMapping("/addPerson")
    public String showAddPersonPage(Model model) {
        PersonForm personForm = new PersonForm();
        model.addAttribute("personForm", personForm);

        return "addPerson";
    }

    @PostMapping("/addPerson")
    public String savePerson(Model model, @ModelAttribute("personForm") PersonForm personForm) {
        String firstName = personForm.getFirstName();
        String lastName = personForm.getLastName();

        if (firstName != null && firstName.length() > 0 && lastName != null && lastName.length() > 0) {
            Person newPerson = new Person(firstName, lastName);
            persons.add(newPerson);

            return "redirect:/personList";
        }

        model.addAttribute("errorMessage", properties.getErrorMessage());
        return "addPerson";
    }
}
