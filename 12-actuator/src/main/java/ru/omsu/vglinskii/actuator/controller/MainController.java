package ru.omsu.vglinskii.actuator.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {

    @ResponseBody
    @RequestMapping(path = "/")
    public String home(HttpServletRequest request) {

        String contextPath = request.getContextPath();
        String host = request.getServerName();

        String endpointBasePath = "/actuator";

        StringBuilder sb = new StringBuilder();
        sb.append("<h2>Sprig Boot Actuator</h2>");
        sb.append("<ul>");
        String url = "http://" + host + ":8090" + contextPath + endpointBasePath;
        sb.append("<li><a href='" + url + "'>" + url + "</a></li>");
        sb.append("<li><a href='" + url + "/health" + "'>" + url + "/health" + "</a></li>");
        sb.append("<li><a href='" + url + "/metrics" + "'>" + url + "/metrics" + "</a></li>");
        sb.append("<li><a href='" + url + "/beans" + "'>" + url + "/beans" + "</a></li>");
        sb.append("<li><a href='" + url + "/env" + "'>" + url + "/env" + "</a></li>");
        sb.append("<li><a href='" + url + "/info" + "'>" + url + "/info" + "</a></li>");
        sb.append("</ul>");

        return sb.toString();
    }

    @ResponseBody
    @RequestMapping(path = "/shutdown")
    public String callActuatorShutdown() {
        String url = "http://localhost:8090/actuator/shutdown";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> requestBody = new HttpEntity<>("", headers);

        String result = restTemplate.postForObject(url, requestBody, String.class);

        return "Result: " + result;
    }

}
