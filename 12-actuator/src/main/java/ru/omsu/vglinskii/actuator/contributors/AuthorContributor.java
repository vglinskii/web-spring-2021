package ru.omsu.vglinskii.actuator.contributors;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class AuthorContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        Map<String,String> data= new HashMap<>();
        data.put("First Name", "Vladimir");
        data.put("Last Name", "Glinskiy");
        data.put("email", "vladimir.glinskiy99@gmail.com");
        builder.withDetail("author", data);
    }
}
