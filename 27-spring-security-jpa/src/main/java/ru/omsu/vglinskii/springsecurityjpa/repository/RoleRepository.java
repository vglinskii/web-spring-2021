package ru.omsu.vglinskii.springsecurityjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.omsu.vglinskii.springsecurityjpa.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
