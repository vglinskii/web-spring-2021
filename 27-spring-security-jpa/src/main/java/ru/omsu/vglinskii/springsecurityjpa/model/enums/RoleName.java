package ru.omsu.vglinskii.springsecurityjpa.model.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
