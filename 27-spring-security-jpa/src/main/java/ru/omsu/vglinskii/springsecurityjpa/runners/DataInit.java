package ru.omsu.vglinskii.springsecurityjpa.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.omsu.vglinskii.springsecurityjpa.model.Role;
import ru.omsu.vglinskii.springsecurityjpa.model.User;
import ru.omsu.vglinskii.springsecurityjpa.model.enums.RoleName;
import ru.omsu.vglinskii.springsecurityjpa.repository.RoleRepository;
import ru.omsu.vglinskii.springsecurityjpa.repository.UserRepository;

import java.util.Arrays;
import java.util.Collections;

@Component
public class DataInit implements ApplicationRunner {
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public DataInit(
            UserRepository userRepository,
            RoleRepository roleRepository
    ) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = userRepository.count();
        long rolesCount = roleRepository.count();
        if(rolesCount == 0) {
            roleRepository.save(new Role(1L, RoleName.ROLE_USER));
            roleRepository.save(new Role(2L, RoleName.ROLE_ADMIN));
        }

        if (count == 0) {
            User user = new User(
                    null,
                    "user",
                    "$2a$10$L1kAquIu.Um45oF2vqMfEOJactPE0rX9W8gy6Qc7RUH/v8mbvUXP.",
                    true,
                    Collections.singletonList(new Role(1L, RoleName.ROLE_USER)));

            User admin = new User(
                    null,
                    "admin",
                    "$2a$10$0inl3LbAzoNnBIveZZBZEuomREa4eG/MAOEfAIh6SbvD4Ol9DvWHy",
                    true,
                    Collections.singletonList(new Role(2L, RoleName.ROLE_ADMIN)));

            userRepository.save(user);
            userRepository.save(admin);
        }

    }
}
