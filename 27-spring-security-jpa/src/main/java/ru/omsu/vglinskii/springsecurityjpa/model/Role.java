package ru.omsu.vglinskii.springsecurityjpa.model;

import org.springframework.security.core.GrantedAuthority;
import ru.omsu.vglinskii.springsecurityjpa.model.enums.RoleName;

import javax.persistence.*;

@Entity
@Table(name = "role",
        uniqueConstraints = {
        @UniqueConstraint(name = "role_uk", columnNames = "name")
})
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", length = 30, nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleName name;

    public Role() {}

    public Role(Long id, RoleName name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name.toString();
    }
}
