package ru.omsu.vglinskii.springsecurityjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.omsu.vglinskii.springsecurityjpa.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
